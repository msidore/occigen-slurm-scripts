# OCCIGEN slurm scripts

A few scripts to run gromacs analysis and run tools on OCCIGEN (Montpellier).

To load GROMACS 2018.6 in OCCIGEN:

    module load intel/18.1
    module load openmpi/intel/2.0.4
    module load gromacs/2018.6

If we handle big files (like my not-so-reasonable few To trajectories) ...
You should partition the directories in which we write those with this command.
The 20 is the number we divide the dir into. 

    lfs setstripe -c 20 .
    
You can check the strip with

    lfs getstripe .
    
This also works with individual files.